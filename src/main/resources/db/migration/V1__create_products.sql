CREATE TABLE products(
    id bigserial NOT NULL,
    sku bigint NOT NULL,
    title varchar(200) NOT NULL,
    description varchar(500),
    style varchar(200),
    price numeric(12,2),
    installments smallint,
    currency varchar(5),
    currency_format varchar(2),
    is_free_shipping boolean NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE product_sizes (
	product_id int8 NOT NULL,
	size_name varchar(3) NOT NULL,
	PRIMARY KEY (product_id, size_name),
	FOREIGN KEY (product_id) REFERENCES products(id)
	);