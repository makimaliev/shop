package kg.edu.spring.shop.controller;

import kg.edu.spring.shop.model.Product;
import kg.edu.spring.shop.payload.ApiResponse;
import kg.edu.spring.shop.payload.ProductRequest;
import kg.edu.spring.shop.payload.ProductsResponse;
import kg.edu.spring.shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @Value("${upload.path}")
    private String path;

    @PostMapping("/add")
    public ResponseEntity<?> addProduct(ProductRequest request, @RequestParam("file") MultipartFile file)
    {
        Product product = productService.createProduct(request, file, path);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("{productId}")
                .buildAndExpand(product.getId()).toUri();
        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "New product created successfully"));
    }

    @GetMapping("/list")
    public ProductsResponse getProducts()
    {
        return productService.findAll();
    }
}
