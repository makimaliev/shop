package kg.edu.spring.shop.service;

import kg.edu.spring.shop.model.Product;
import kg.edu.spring.shop.payload.ProductRequest;
import kg.edu.spring.shop.payload.ProductsResponse;
import kg.edu.spring.shop.repository.ProductRepository;
import kg.edu.spring.shop.util.ImageUploader;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    @Transactional
    public Product createProduct(ProductRequest request, MultipartFile file, String path)
    {
        ImageUploader.uploadImage(file,path + request.getSku());

        Product product = new Product();
        product.setSku(request.getSku());
        product.setTitle(request.getTitle());
        product.setDescription(request.getDescription());
        product.setAvailableSizes(request.getAvailableSizes());
        product.setStyle(request.getStyle());
        product.setPrice(request.getPrice());
        product.setInstallments(request.getInstallments());
        product.setCurrency(request.getCurrency());
        product.setCurrencyFormat(request.getCurrencyFormat());
        product.setIsFreeShipping(request.getIsFreeShipping());
        return productRepository.save(product);
    }

    public ProductsResponse findAll()
    {
        ProductsResponse response = new ProductsResponse();
        response.setProducts(productRepository.findAll());
        return response;
    }

}
