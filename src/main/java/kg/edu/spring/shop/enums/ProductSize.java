package kg.edu.spring.shop.enums;

public enum ProductSize {
    XS,
    S,
    M,
    L,
    XL,
    XXL
}
