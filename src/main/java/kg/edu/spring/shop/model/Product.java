package kg.edu.spring.shop.model;

import kg.edu.spring.shop.enums.Currency;
import kg.edu.spring.shop.enums.ProductSize;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "products")
@Data
public class Product extends BaseEntity {

    private Long sku;

    @Size(max = 200)
    private String title;

    private String description;

    @ElementCollection(targetClass = ProductSize.class)
    @CollectionTable(name = "product_sizes",
        joinColumns = @JoinColumn(name="product_id"))
    @Enumerated(EnumType.STRING)
    @Column(name="size_name")
    private Set<ProductSize> availableSizes;

    @Size(max = 200)
    private String style;

    private BigDecimal price;

    private int installments;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    private Currency currency;

    @Size(max = 2)
    private String currencyFormat;

    private Boolean isFreeShipping;
}
