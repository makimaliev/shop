package kg.edu.spring.shop.util;

import org.imgscalr.Scalr;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageUploader {

    public static void uploadImage(MultipartFile file, String imageName){
        try {
            BufferedImage img = ImageIO.read(file.getInputStream());
            BufferedImage mainImage = Scalr.resize(img, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, 333, Scalr.OP_ANTIALIAS);
            BufferedImage thumbImage = Scalr.resize(img, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, 120, Scalr.OP_ANTIALIAS);
            File mainImageFile = new File(imageName + "_1.jpg");
            File thumbImageFile = new File(imageName + "_2.jpg");
            ImageIO.write(mainImage, file.getContentType().split("/")[1], mainImageFile);
            ImageIO.write(thumbImage, file.getContentType().split("/")[1], thumbImageFile);
        } catch (IOException e) {
            e.printStackTrace();
            //return new ResponseEntity("Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
