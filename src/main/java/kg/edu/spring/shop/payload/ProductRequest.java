package kg.edu.spring.shop.payload;

import kg.edu.spring.shop.enums.Currency;
import kg.edu.spring.shop.enums.ProductSize;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
public class ProductRequest {

    @NotNull
    private Long sku;

    @NotBlank
    @Size(max = 200)
    private String title;

    private String description;

    private Set<ProductSize> availableSizes;

    @Size(max = 200)
    private String style;

    @NotNull
    private BigDecimal price;

    private int installments;

    private Currency currency;

    private String currencyFormat;

    private Boolean isFreeShipping;
}
