package kg.edu.spring.shop.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import kg.edu.spring.shop.model.Product;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ProductsResponse {

    @JsonProperty("products")
    private List<Product> products;

}
